[General]
version = 2
blueprintrepositories = https://invent.kde.org/packaging/craft-blueprints-kde.git|master|

[dev-utils/nsis]

#
# the below matches the Qt 6 Window CI image package list
#

# For everything that uses MIME information
[libs/shared-mime-info]

# Everything wants Qt 6 and KDE Frameworks
[libs/qt6]
[kde/frameworks]

# kirigami-addons is needed by a lot of apps
[kde/unreleased/kirigami-addons]

# Everything in Frameworks also needs Doxygen for QCH generation
[dev-utils/doxygen]

# ECM
[dev-utils/icoutils]

# KArchive
[libs/libarchive]
[libs/liblzma]

# KI18n
[libs/gettext]
[data/iso-codes]

# Solid
[dev-utils/flexbison]

# Prison
[libs/qrencode]
[libs/libdmtx]
[libs/zxing-cpp]

# KDocTools
[libs/libxslt]
[libs/libxml2]
[data/docbook-dtd]
[data/docbook-xsl]
[perl-modules/uri-url]
[perl-modules/xml-parser]

# KNotification
[dev-utils/snoretoast]

# KWallet
[libs/gcrypt]

# KActivities
[libs/boost/boost-headers]

# KCodecs
[dev-utils/gperf]

# Purpose
#kdesupport/qca - cannot be cached due to not being relocatable

# Breeze Icons
[python-modules/lxml]

# KFileMetaData
[libs/taglib]

# libkexiv2
[libs/exiv2]

# libkdcraw
[libs/libraw]

# KStars
[libs/eigen3]
[libs/wcslib]
[libs/libxisf]
[libs/libnova]
[libs/cfitsio]
# Does not support Qt6 yet
# [libs/stellarsolver]

# Minuet
[libs/glib]
[libs/fluidsynth]

# KCalCore
[libs/libical]

# Analitza
[libs/glew]

# Okular
[libs/ghostscript]
[qt-libs/poppler]
[libs/ebook-tools]
[libs/chm]
[libs/discount]
[libs/djvu]
[libs/libspectre]

# Kiten
[libs/mman]

# Lokalize
[libs/hunspell]

# KDevelop
[libs/llvm]

# Labplot
[libs/gsl]

# Skrooge
[dev-utils/pkg-config]

# Elisa
[binary/vlc]

# Calligra
[libs/boost/boost-system]

# Digikam, Kdenlive
[libs/ffmpeg]

# Digikam
[libs/sqlite]
[libs/x265]
[libs/tiff]
[libs/expat]
[libs/lcms2]
[libs/opencv]
[libs/lensfun]
[libs/openal-soft]
[libs/pthreads]
[libs/libjpeg-turbo]

# Kdenlive
[libs/mlt]
[libs/drmingw]
[kde/kdenetwork/kio-extras]
[kde/plasma/breeze]
# DrKonqi needs PolKit Qt6, which we don't have in Craft
# [kde/plasma/drkonqi]

# NeoChat, Ruqola, ...
[qt-libs/qtkeychain]

# Ruqola
[kde/libs/ktextaddons]

# NeoChat
#TODO no Qt6 support in Craft yet
#qt-libs/libquotient
#qt-libs/qcoro
#libs/cmark

# RKWard
[binary/r-base]

# libqgit2
[libs/libgit2]

# Dolphin
[qt-libs/phonon]
[kde/kdemultimedia/ffmpegthumbs]