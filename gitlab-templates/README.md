# Our GitLab CI/CD Pipelines

This folder has templates that can be included in your project's `.gitlab-ci.yml` file by using their full file path
in this repository and the [`include:` keyword](https://invent.kde.org/help/ci/yaml/index.md#includeproject). For example:

```yaml
include:
  - project: sysadmin/ci-utilities
    file:
      - /gitlab-templates/freebsd-qt6.yml
      - /gitlab-templates/linux-qt6.yml
```

For further details about our pipeline infrastructure, see [Continuous Integration System](https://community.kde.org/Infrastructure/Continuous_Integration_System).

## CI Pipelines

### General
* json-validation.yml - Validates JSON files in the project
* reuse-lint.yml - Validates [REUSE compliance](https://community.kde.org/Policies/Licensing_Policy) of the project

### Linux
* linux.yml, linux-qt6.yml - Builds and runs unit tests for the project with Qt 5 and Qt 6 respectively
* linux-static.yml, linux-qt6-static.yml - Creates statically linked builds with Qt 5 and Qt 6 respectively

### FreeBSD
* freebsd.yml, freebsd-qt6.yml - Builds and runs unit tests for the project with Qt 5 and Qt 6 respectively

### Windows
* windows.yml, windows-qt6.yml - Builds and runs unit tests for the project with Qt 5 and Qt 6 respectively
* windows-static.yml - Creates statically linked builds with Qt 5

### Android
* android.yml, android-qt6.yml - Cross-builds the project with Qt 5 and Qt 6 respectively

## CD Pipelines

### Linux
* craft-appimage.yml, craft-appimage-qt6.yml - Builds an AppImage for the project
* flatpak.yml - Builds a Flatpak for the project

### Windows
* craft-windows-mingw64.yml, craft-windows-mingw64-qt6.yml - Builds the project with MingW64 and creates some installers
* craft-windows-x86-64.yml, craft-windows-x86-64-qt6.yml - Builds the project with MSVC and creates some installers
* craft-windows-base.yml - Common code of the Windows pipelines
* craft-windows-appx-qt5.yml, craft-windows-appx-qt6.yml - Provides a job for publishing in the Microsoft Store

### macOS
* craft-macos-arm64.yml, craft-macos-arm64-qt6.yml - Builds the project for ARM64 and package to DMG
* craft-macos-x86-64.yml, craft-macos-x86-64-qt6.yml - Builds the project for x86 and package to DMG
* craft-macos-base.yml - Common code of the macOS pipelines

### Android
* craft-android-apks.yml, craft-android-qt6-apks.yml -
Builds signed APKs for the project for ARM32, ARM64, and Intel 64-bit with Qt 5 and Qt 6 respectively.
Publishes APKs in KDE's F-Droid repositories.
* craft-android-appbundle.yml - Repackages the platform-specific APKs (Qt 5) in one [Android App Bundle](https://developer.android.com/guide/app-bundle) (AAB)
* craft-android-qt5-googleplay-apks.yml - Submits APKs to Google Play.

## Website Pipelines
* website-hugo.yml
* website-sphinx-app-docs.yml
* website-base.yml - Common code of the website pipelines

## Details

### Craft Pipelines

All pipelines using Craft assume that the name of the project's Craft blueprint matches
the name of the project's repository ([CI_PROJECT_NAME](https://invent.kde.org/help/ci/variables/predefined_variables.md)).

The pipelines can be customized with a `.craft.ini` file in the project's root
folder to override the defaults of Craft and the Craft blueprints. Some examples which
illustrate the syntax:

```
[BlueprintSettings]
kde/applications/kate.packageAppx = True
kde/applications/konsole.version = master
```
This `.craft.ini` could be used by Kate. Setting the `packageAppx` option to `True` for Kate
enables the creation of APPX packages for Kate. And setting `version` of `konsole` to `master`
tells Craft that the `master` version of Konsole should be used for Kate.

You should only use a specific version of a dependency in `.craft.ini` if really needed.
Using the default values provided by Craft and the default config has the benefit that the
Craft binary cache is used saving system resources (and build time).

If want to override a default version prefer individual blueprints like `kde/frameworks/kirigami`
over entiere namespace like `kde/frameworks`.

```
[android-arm-clang-BlueprintSettings]
libs/qt5/qtsvg.featureArguments = -feature-qgraphicssvgitem

[android-arm64-clang-BlueprintSettings]
libs/qt5/qtsvg.featureArguments = -feature-qgraphicssvgitem

[android-x86_64-clang-BlueprintSettings]
libs/qt5/qtsvg.featureArguments = -feature-qgraphicssvgitem
```
This `.craft.ini` shows how to specify special settings for the Android builds.

### Android CD Pipelines

The CD pipelines for Android build signed or unsigned APKs and AABs and publish
them in our F-Droid repositories and on Google Play. Signed APKs are only created
for a few branches, typically the latest release branch for stable builds and the
master branch for nightly builds. Likewise, publication happens only for certain
branches.

To enable signing and publication for your project you need to edit the [project
settings](signing/README.md) of the corresponding CI notary services.

## Builder deployment instructions

### Linux Hosts

In order to best support building both the images that run our CI builds, as well as build Flatpak bundles, a specialised setup is required.
Specifically, we must use Podman in a rootless setup, and configure Gitlab Runner to pass specific flags to Podman (acting in Docker compatibility mode).

Because Gitlab Runner only supports Podman 4.2 and later, we must use a comparatively modern distribution. At the time of writing this means deploying
either Debian Bookworm or Alma Linux 9. The version of Podman in Ubuntu 22.04 is too old and therefore cannot be used.

We also use our Linux hosts to run virtual machines that host our Windows and FreeBSD builds.

```
# Install any pending system updates
apt update
apt upgrade

# Add the Gitlab Runner repository then install Gitlab Runner
apt install ca-certificates curl gnupg
curl -fsSL https://packages.gitlab.com/runner/gitlab-runner/gpgkey | gpg --dearmor > /usr/share/keyrings/runner_gitlab-runner-archive-keyring.gpg
curl -fsSL "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/config_file.list?os=debian&dist=bookworm&source=script" > /etc/apt/sources.list.d/gitlab-runner.list

# Ensure that we always use the Gitlab repository to provide gitlab-runner (see https://docs.gitlab.com/runner/install/linux-repository.html#apt-pinning)
vim /etc/apt/preferences.d/pin-gitlab-runner.pref

# Install Podman, KVM and Gitlab Runner
# uidmap is not dragged in by Podman but is required by it to run rootless
# containers-storage is not dragged in by Podman but without it the vfs storage driver will be used which is both inefficient storage wise and very slow
# slirp4netns is not dragged in by Podman but is required by it to setup networking for rootless containers
# dnsmasq is not dragged in by libvirt-daemon-system but without it the default NAT network cannot be started as dnsmasq is used to provide DHCP services
apt update
apt install podman uidmap containers-storage slirp4netns apparmor
apt install qemu-kvm libvirt-daemon-system dnsutils dnsmasq
apt install gitlab-runner

# Disable the system wide dnsmasq instance
# This is not needed by libvirtd, which starts it's own instances for the bridges it maintains
systemctl disable dnsmasq.service
systemctl stop dnsmasq.service

# Permit Gitlab Runner to make use of user namespaces
usermod --add-subuids 100000-165536 gitlab-runner
usermod --add-subgids 100000-165536 gitlab-runner

# KWin Specific: enable the kernel vgem module to provide DRI services needed to support KWin's unit tests
# As DRI devices are locked down by default, we need to ensure that the user in the rootless Podman containers have permission to utilise them
# This is done through adding additional file permissions
modprobe vgem
echo "vgem" > /etc/modules-load.d/vgem.conf
vim ~root/set-dri-perms.sh
chmod +x ~root/set-dri-perms.sh
~root/set-dri-perms.sh

# Create directories to store our CI artifacts and compiler caches and ensure they are owned by uid 1000 in the containers we spawn.
# As uid 0 in the container is mapped to the Gitlab Runner, uid 100999 on the host will correspond to uid 1000 in the container (as host 100000 = container 1)
# We also create a directory for our Podman images and containers to be stored in. This is specific to our builders as the root partition is small while /mnt has the bulk of the storage.
# As we're running rootless ownership of that folder must be transferred to the user that Gitlab Runner runs as too
mkdir /mnt/containers/ /mnt/artifacts/ /mnt/caches/
chown gitlab-runner:gitlab-runner /mnt/containers/
chown 100999:100999 /mnt/artifacts/ /mnt/caches/

# Allow gitlab-runner to 'linger' which will ensure the user systemd session is around to spawn Podman for Gitlab Runner
loginctl enable-linger gitlab-runner

# Switch over to the Gitlab Runner account now to complete remaining setup
machinectl shell gitlab-runner@

# Make use of the dedicated storage space we setup earlier
mkdir -p .local/share/
ln -s /mnt/containers/ .local/share/containers

# Ensure we are using the correct storage driver of "overlay" - you don't want to see vfs here
podman info | grep graphDriverName

# Switch on Podman then return to root
systemctl --user enable --now podman.socket
systemctl --user start podman.socket
exit

# Download the ISO images we need to install FreeBSD and Windows
# For Windows we make use of Windows Server 2022 Datacentre Edition, which should be downloaded seperately from Microsoft and uploaded via SFTP
cd /var/lib/libvirt/images/
wget https://download.freebsd.org/releases/amd64/amd64/ISO-IMAGES/14.0/FreeBSD-14.0-RELEASE-amd64-disc1.iso
wget https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/virtio-win-0.1.240-1/virtio-win-0.1.240.iso
```

The contents of ~root/set-dri-perms.sh should be:
```
#!/bin/bash
setfacl -m u:100999:rw- /dev/dri/card0
setfacl -m u:100999:rw- /dev/dri/renderD128
```

Once the FreeBSD VM is deployed, the content of ~root/forward-freebsd-iptables.sh should be as follows:
(Substituing $FREEBSD_IP for the actual IP address which can be obtained from the VM itself or using a libvirt utility such as virt-manager)
```
#!/bin/bash
iptables -t nat -A PREROUTING -i eno1 -p tcp --dport 2022 -j DNAT --to $FREEBSD_IP:22
iptables -I FORWARD -d $FREEBSD_IP/32 -i eno1 -p tcp -m tcp --dport 22 -j ACCEPT
```

### FreeBSD Hosts

Once a Linux host has been deployed, a FreeBSD VM can be deployed on top of it.

In order to support ephemeral builds and to ensure we don't have issues such as running out of disk space we make use of the Podman support included in FreeBSD 14.0.
As such, the use of any other version of FreeBSD is not supported.

File system wise, the usage of ZFS as the system file system is mandatory to allow Podman to utilise it to thin-provision the containers (FreeBSD Jails) it launches.

When deploying the VM, no particularly special care needs to be taken as virt-manager by default will choose paravirtualised networking and storage devices.

When installing FreeBSD:
- When selecting the system components to install, select only 'lib32'
- Use ZFS as the file system, with no swap space configured.
- When selecting the services to be started, ensure only 'sshd' and 'dumpdev' are enabled (the default)
- For system hardening, enable 'read_msgbuf', 'random_pid', 'clean_tmp', 'disable_syslogd' and 'disable_ddtrace'
- Don't add any users to the system

At the end of the installation accept the option to perform additional confirmation and run:
```
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
```

Once the system has rebooted, login via SSH and lock down SSH as you feel appropriate (installing SSH public keys, disabling password login, only allowing non-password root login, etc)

```
# Install any updates, along with text editor and process monitoring tools
pkg upgrade
pkg install htop vim-tiny

# Disable default FreeBSD repository
mkdir -p /usr/local/etc/pkg/repos/ && echo "FreeBSD { enabled: no }" > /usr/local/etc/pkg/repos/FreeBSD.conf

# Add our custom repository (See contents below)
vim /usr/local/etc/pkg/repos/ci_pkgs.conf

# Install Podman, Gitlab Runner and Git itself
pkg install podman gitlab-runner git

# Setup networking to be suitable for Podman
cp /usr/local/etc/containers/pf.conf.sample /etc/pf.conf
sed -ie 's,ix0,vtnet0,' /etc/pf.conf
echo 'pf_load="YES"' >> /boot/loader.conf
echo 'net.pf.filter_local=1' >> /etc/sysctl.conf.local
kldload pf
sysctl net.pf.filter_local=1
service pf enable
service pf start

# Ensure other container infrastructure is in place:
echo "fdesc   /dev/fd         fdescfs         rw      0       0" >> /etc/fstab
mount -a

# Create a ZFS storage mount for containers
zfs create -o mountpoint=/var/db/containers zroot/containers

# Enable Podman to start
sysrc podman_service_enable=YES
service podman_service start

# Ensure our Gitlab Runner helper image is available
podman pull invent-registry.kde.org/sysadmin/ci-images/freebsd14-gitlab-helper:latest

# Register the Gitlab Runner, then edit it's configuration to be suitable
gitlab-runner register [args]
vim /usr/local/etc/gitlab-runner/config.toml

# Setup folders for our CI jobs to use
mkdir /mnt/artifacts/ /mnt/caches/
chown 1001:1001 /mnt/artifacts/ /mnt/caches/

# Perform basic setup of Gitlab Runner to work with Podman
# Given we are running as root make sure you don't setup a shell executor otherwise you will give CI jobs root on your system!
sysrc gitlab_runner_enable=yes
sysrc gitlab_runner_user=root
sysrc gitlab_runner_group=wheel
service gitlab_runner start

# Ensure that LRO is disabled, otherwise network performance will be very painful in containers
# See https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=273046
sysrc ifconfig_vtnet0="DHCP -lro"
```

Contents of ci_pkgs.conf:
```
ci_pkgs: {
        url: "https://cdn.kde.org/freebsd/140amd64",
        signature_type: "none",
        enabled: yes,
        priority: 10
}
```

Essential lines to be included in gitlab-runner config.toml:
```
  [runners.docker]
    host = "unix:///var/run/podman/podman.sock"
    helper_image = "invent-registry.kde.org/sysadmin/ci-images/freebsd14-gitlab-helper:latest"
```

In the future it won't be needed to switch to the CI packages repository, once the necessary updates to Podman and Gitlab Runner have landed in the FreeBSD repos.

### Windows Hosts

Once a Linux host has been deployed, a Windows VM can be deployed on top of it. To keep these secure, they are only accessible on the NAT network created by libvirt.

At this time we are deploying Windows Server 2022 as our standard base operating system for Windows build hosts.

This was chosen because in order to ensure we are not resource limited or subject to the overhead of virtualisation we need to run using 'process' isolation mode. In
order to run in 'process' isolation mode however the version of Windows on the host and in the container must match. As Microsoft only provides container images for
the server editions of Windows, we therefore need to use a corresponding Windows Server edition as the host OS.

Further, prior to Windows Server 2022 it was also required that the build revision of Windows between the host and container matched exactly. As the build revision
regularly changes as updates are shipped by Microsoft, the act of installing updates could break the setup.

We therefore use Windows Server 2022 exclusively on the CI builders.

When deploying the VM, care must be taken as by default virt-manager will specify the use of full virtualised storage and networking devices. These will reduce the
performance of the final installed system, so once you have reached the end of the VM creation process in virt-manager you should select the option to customise
the machine before commencing installation.

Once the customisation screen opens:
- Change the networking device model to 'virtio'
- Change the disk device model to 'virtio' as well (you can ignore the CDROM drives as those cannot use virtio)
- Add an additional CDROM device, and point it to the virtio ISO we downloaded above

Once you commence installation it should fail to boot as the VM (no matter what you try) will try to boot off the virtio iso instead of the Windows Server ISO.
To correct this simply shut the VM down, open it's settings and correct the boot order to have the VirtIO Disk followed by the Windows ISO then boot it up again.

Once in the installer, you will be asked to select the edition of Windows Server you want to install. As we don't run Windows on our local machines, and don't
have direct connectivity to the machines administering Server Core installations remotely tends to be difficult. It is therefore recommended to select a Desktop
Experience version of Server 2022.

At the next step you will find no storage devices are available - which is normal as we've opted to use paravirtualised 'virtio' storage. The drivers for this
can be found on the virtio ISO we attached earlier. Note that within each driver folder is an architecture folder, and the installer requires that you select
the folder that contains the driver (it won't scan for them). It is recommended to install the networking VirtIO driver at the same time.

You can then proceed with installing Windows as normal.

Once you've logged in for the first time, Windows will prompt you as to whether you want to make your "device" discoverable on the network. As this influences
the type of network selected in the firewall, answer yes to this question.

You will then want to install the remaining drivers including the QEMU guest additions. This can be done by browsing to the VirtIO ISO and locating the installer
named "virtio-win-guest-tools". Proceed through the installers, and reboot if prompted to do so.

Next we will want to enable Remote Desktop as it provides a better remote access experience compared to SPICE. This can be done within Windows settings.
As members of the "Administrators" group are allowed to use RDP by default, no additional action is needed to allow access via RDP.

At this point you can now disconnect from the virt-manager provided console and connect using RDP instead. If you wish to continue using virt-manager you should
enable automatic resizing of the guest display, which can be done from View > Scale Displays in virt-manager. For RDP, as the machine is not exposed to the public
internet, it is recommended to use Remmina as it has native support for connecting to hosts over SSH tunnels.

With Windows itself now setup, we can turn our attention to configuring it to act as a Gitlab Runner with Docker.

First we should install Docker, using a script helpfully provided by Microsoft. As part of this it will install several Windows "features" which will require
the system to be rebooted. This needs to be run from an elevated Powershell terminal:
```
Invoke-WebRequest -UseBasicParsing "https://raw.githubusercontent.com/microsoft/Windows-Containers/Main/helpful_tools/Install-DockerCE/install-docker-ce.ps1" -o install-docker-ce.ps1
.\install-docker-ce.ps1
```

Once that has completed, in order to support building containers on the host we will also need to install Git itself. This should be installed from the Git
website, accepting the options to add Git itself (but not the other tools) to PATH, installing system wide and disabling support for symbolic links. All other
defaults can be accepted.

Now we need to install Gitlab Runner itself. To do so, head over to the Gitlab website and locate the latest build of gitlab-runner.exe. Download this and copy it
over to C:\Gitlab\ (creating the folder if needed). Open the properties of the program once it is in place and ensure that it is not blocked (clicking Unblock if needed).
Then open an elevated Powershell prompt, change to C:\Gitlab and run `gitlab-runner.exe install` which should complete the installation process.

Next we need to setup an account to run the Gitlab Runner under, as by default it will have set itself up to run under the SYSTEM account, which is too highly privileged.

To do so open "Computer Management" (run compmgmt.msc) and use "Local Users and Groups" to create a user named Gitlab and a group named Docker. Make sure the user Gitlab
is the sole member of the group you create. When creating the user ensure you opt for a local only account (ignoring any prompts for a Microsoft Account) and
make a note of the password you set for later.

Then switch over to "Services" and stop the Docker service. With the Docker service stopped, open C:\ProgramData\docker\config\daemon.json using Notepad and replace
it's contents with the following. This allows members of the "Docker" group you created to connect to the Docker service, as otherwise only Administrators will be
allowed to do so (which the Gitlab account we just created isn't, so without this it would be unable to carry out Docker actions)
```
{
    "hosts":  [
                  "npipe://"
              ],
    "group": "Docker"
}
```

With this completed, you can return to the "Services" section of "Computer Management" and start the Docker service back up again.

Still within the Services area, locate the gitlab-runner service and open it's properties. If it is already running (it shouldn't be) stop it then switch to the Log On tab.
Set this to use a specific account, and provide the Gitlab account username and password you created earlier. Once that change is applied it may inform you it has granted
a privilege to the Gitlab user to run services which is normal. Finally ensure the service start type is set to Automatic and start the Gitlab Runner service.
